// dependencies
const express = require("express");
const restful = require('node-restful');
const mongoose = restful.mongoose;

const Member = mongoose.Schema({
    first_name: {type: String, required: true},
    last_name: {type: String, required: true},
    creation_date: {type: Date, default: Date.now},
    books_borrowed: [
	{
	    id: {type: Number, required: true},
	    date_borrowed: {type: Date, required: true},
	    date_returned: {type: Date},
	    returned: {type: Boolean, required: true}
	}],
    address: {type: String, required: true},
    phone_number: {type: String}
});

module.exports = restful.model('Member', Member);
