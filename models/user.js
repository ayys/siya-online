// dependencies
const express = require("express");
const mongoose = require('mongoose');

const User = mongoose.Schema(
    {
	username: {type: String, required: true},
	password: {type: String, required: true},
	first_name: {type: String, required: true },
	last_name: {type: String, required: true},
	permissions:{type: Number, required: true}
    }
);
module.exports = mongoose.model('User', User);
