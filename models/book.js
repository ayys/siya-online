// dependencies
const express = require("express");
const restful = require('node-restful');
const mongoose = restful.mongoose;

const Book = mongoose.Schema({
    accession_number: {type: Number,
		       required: true,
		       index: {unique: true, dropDups: true}},
    accessioned_date: {type: Date, default: Date.now,},
    call_number: {type: String, required: true,},
    title: {type: String, required: true,},
    authors: [{
	name: {type: String}
    }],
    no_of_pages: {type: Number, required: true,},
    series: {type: String},
    edition: {type: String},
    price: {type: String},
    volume: {type: String},
    isbn: {type: String},
    state: {type: String},
    donor: {type: String},
    publisher: [{
	name: {type: String, required: true},
	year: {type: String, required: true},
	place: {type: String, required: true},	
    }],
    votes: {type: String},
    views: {type: String},
});

module.exports = restful.model('Book', Book);
