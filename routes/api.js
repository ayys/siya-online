// dependencies
const express = require("express");
const restful = require('node-restful');
const mongoose = restful.mongoose;
const Book = require('../models/book');
const config = require('../config');
const jwt = require('jsonwebtoken');

const router = express.Router();

Book.methods(['get', 'post', 'put', 'delete']);

Book.before('get', authenticateUser);
Book.before('post', authenticateUser);
Book.before('put', authenticateUser);
Book.before('delete', authenticateUser);

function authenticateUser(req, res, next)
{
    // authenticate user
    const token = req.headers['token'];
    const username = req.headers['username'];
    jwt.verify(token, config.secret, (err, payload) => {
	if (err) next(err);
	else if (payload.username == username)
	    next();
	else
	    next(new Error("username doesn't match"));
    });
}

Book.register(router, '/books');

module.exports = router;
