const express = require("express");
const restful = require('node-restful');
const mongoose = restful.mongoose;
const Member = require('../models/member');

const router = express.Router();

Member.methods(['get', 'post', 'put', 'delete']);

Member.before('get', authenticateUser);
Member.before('post', authenticateUser);
Member.before('put', authenticateUser);
Member.before('delete', authenticateUser);

function authenticateUser(req, res, next)
{
    // authenticate user
    const token = req.headers['token'];
    const username = req.headers['username'];
    jwt.verify(token, config.secret, (err, payload) => {
	if (err) next(err);
	else if (payload.username == username)
	    next();
	else
	    next(new Error("username doesn't match"));
    });
}


Member.register(router, '/members');

module.exports = router;
