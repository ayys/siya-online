const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../models/user');
const config = require('../config');

router.post('/register', (req, res) => {
    // check if username exists
    User.findOne({username: req.body.username}, (err, user) => {
	if (user != undefined)
	    res.json({"msg": "username exists"});
	else {
	    var newUser = new User({
		username: req.body.username,
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		permissions: req.body.permissions
	    });
	    bcrypt.hash(req.body.password, 10, function(err, hash) {
		newUser.password = hash;
		newUser.save((err, user) => {
		    if (err) {
			res.json({"msg": "User creation failed!"});
		    }
		    else res.json({"msg": "User creation successful"});
		});
	    });
	    
	}
    });
});

router.post("/login", (req, res) => {
    User.findOne({username: req.body.username}, (err, user)=> {
	if (err) res.json({"msg": err});
	else {
	    bcrypt.compare(req.body.password, user.password, (err, resp) => {
		if (resp == true) {
		    const token  = jwt.sign({username: user.username}, config.secret);
		    res.json({"token": token});
		}
		else res.json({'msg': 'login failed'});
	    });
	}
    });
});

module.exports = router;
