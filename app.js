// dependencies
const dotenv = require('dotenv');
dotenv.config();
const express = require("express");
const restful = require('node-restful');
const bodyParser = require('body-parser');
const mongoose = restful.mongoose;
// dependencies for authentication
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


const config = require('./config');
// local dependencies


// connect to database
mongoose.connect(config.database);

// app stuff
const app = express();

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

// api stuff
const api_route = require('./routes/api');
const user_route = require('./routes/users');

// routing
app.use('/api', api_route);

app.use('/user', user_route);


// server start listenning.... 

app.listen(3000, (err) => console.log('listening at localhost:3000'));
